// Get today's date as dd/mm/yyyy format
export const getTodaysDate = () => {
  let today = new Date();
  const dd = String(today.getDate()).padStart(2, "0");
  const mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
  const yyyy = today.getFullYear();
  return dd + "/" + mm + "/" + yyyy;
};

function scaleImage(dataUrl, scaleRatio, imageType, imageArguments) {
  "use strict";
  var image, oldWidth, oldHeight, newWidth, newHeight, canvas, ctx, newDataUrl;

  return new Promise((resolve, reject) => {
    // Create a temporary image so that we can dimensions of new image.
    image = new Image();
    image.onload = function () {
      oldWidth = image.width;
      oldHeight = image.height;
      newWidth = Math.floor(oldWidth * scaleRatio);
      newHeight = Math.floor(oldHeight * scaleRatio);

      // Create a temporary canvas to draw the downscaled image on.
      canvas = document.createElement("canvas");
      canvas.width = newWidth;
      canvas.height = newHeight;

      // Draw the scaled image on the canvas and trigger the callback function.
      ctx = canvas.getContext("2d");
      ctx.drawImage(image, 0, 0, newWidth, newHeight);
      newDataUrl = canvas.toDataURL(imageType, imageArguments);
      resolve(newDataUrl);
    };
    image.src = dataUrl;
  });
}

// Image compression to reduce file size
export async function compressImageDataUrl(oldDataUrl, type) {
  return await scaleImage(oldDataUrl, 0.5, type, 0.7);
}

export const spacify = (str, blocksSize) => {
  if (!str) return "";
  blocksSize = blocksSize || 4;
  const reg = new RegExp(".{" + blocksSize + "}", "g");

  // Replace everything that is not letters and numbers
  const v = str.replace(/[^\d\w]/g, "");
  const withBlockSpacings = v.replace(reg, (a) => `${a} `);
  const withSpacesRemovedAtTheEnd = withBlockSpacings.replace(/[^\d\w]+$/, "");
  return withSpacesRemovedAtTheEnd.toUpperCase();
};
