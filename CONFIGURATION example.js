/**
 * [ ORGANISATIONS ]
 *
 * Décrivez ici votre organisation ou vos différentes organisations pour lesquelles faire des notes de frais.
 *
 * - name: Nom de l'asso
 *
 * - legalAddress: Adresse de l'asso officielle. Ajoutez "<br/>" pour sauter une ligne.
 *
 * - correspondenceAddress: Adresse de correspondance, qui doit être utlisée si on veut envoyer la
 * note de frais par courrier. Ajoutez "<br/>" pour sauter une ligne.
 *
 * - color: La couleur de votre organisation, en mode RGB (les trois valeurs sont séparées par des virgules).
 * Vous pouvez utiliser https://htmlcolorcodes.com/fr/ pour créer votre couleur.
 *
 * - email: L'email à qui envoyer la note de frais par la suite.
 */
export const organizations = [
  {
    name: "Alternatiba",
    legalAddress: "20 rue des Cordeliers<br />64100 Bayonne",
    correspondenceAddress: "20 rue des Cordeliers<br />64100 Bayonne",
    color: "61, 129, 72",
    email: "compta@alternatiba.eu",
  },
  {
    name: "ANV-COP21",
    legalAddress: "20 rue des Cordeliers<br />64100 Bayonne",
    correspondenceAddress: "20 rue des Cordeliers<br />64100 Bayonne",
    color: "106, 30, 8",
    email: "compta@anv-cop21.org",
  },
  // //*** Celles là sont des tests ***//
  // {
  //   name: "Amis de la Terre France",
  //   legalAddress: "47 avenue Pasteur,<br />93100 Montreuil",
  //   correspondenceAddress: "47 avenue Pasteur,<br />93100 Montreuil",
  //   color: "88, 176, 66",
  //   email: "secretariat@amisdelaterre.org",
  // },
  // {
  //   name: "Greenpeace France",
  //   legalAddress: "13 rue d’Enghien,<br />75010 Paris",
  //   correspondenceAddress: "13 rue d’Enghien,<br />75010 Paris",
  //   color: "115, 190, 40",
  //   email: "contact@greenpeace.fr",
  // },
];

/**
 * [ CATÉGORIE PAR DÉFAUT ]
 *
 * Les informations de cette catégorie seront affichées si jamais la catégorie de remboursement demandée par
 * l'utilisateur·ice ne correspond à aucune catégorie connue (listée dans la liste ci-dessous).
 *
 * @see categories
 */
export const defaultCategoryInfo = {
  attachments: [
    "Une facture au nom de Alternatiba ou ANV-COP21, ou tout justificatif de paiement que je peux avoir sur mes dépenses",
  ],
  tips: [
    "Si tu as une facture, vérifie bien que l'adresse correspond à celle de l'asso indiquée sur la note de frais.",
  ],
};

/**
 * [ CATÉGORIES/TYPES DE REMBOURSEMENT ]
 *
 * Les catégories / types de remboursements classiques. Ces infos seront affichées
 * en tant que "liste de choses à ne pas oublier" pour l'utilisateur·ice lors du téléchargement de la note de frais.
 *
 * - name: Le nom du type de remboursement
 *
 * - attachments: Les différentes pièces à joindre à la note de frais pour ce type de remboursement particulier
 *
 * - tips: Différents conseils que l'on peut donner à l'utilisateur·ice à propos de ce type de remboursement
 *
 * - template: Le modèle de base qui sera proposé à l'utilisatuer·ice dans le champ "Description" quand la catégorie est sélectionnée
 *
 * - additionalInputFiels : liste de champs additionnels d'input
 *      - inputType : le type du champ
 *          - un champ number est affiché number est avec <th> défini par le champ label
 *          - un champ select est défini par le label et la listes des couples (name / value)
 *      - key : le nom de la clé qui va être utilisé pour stocker la variable dans le code
 *      - label : le nom du champ affiché sur la page
 *
 * - amountFormula : défini si le montant est entré manuellement ou non :
 *      - 'manual' si le montant doit être entré manuellement (champ input number)
 *      - objet de type function qui prend entrée les clé des champs à utiliser
 *
 * Si une catégorie n'a aucune pièce jointe et aucun conseil de mentionné, alors les pièces jointes et
 * conseils par défaut seront affichés (voir plus haut pour modifier les conseils par défaut).
 *
 * @see defaultCategoryInfo
 */
export const categories = [
  /* 08-02-2024 - Adrien - Suppression du remboursement aux frais réels pour le remplacer pour les frais kilométriques.
  {
    name: "Essence voiture",
    attachments: ["La carte grise du véhicule", "Un ticket de plein d'essence si disponible", "Une estimation du coût du voyage en carburant (sur Mappy ou ViaMichelin)"],
    tips: [
      "J'ai mentionné la date, le lieu de départ et d'arrivée, et s'il s'agit d'un aller-retour ou non.",
      "J'ai recherché sur " +
        "<a href='https://fr.mappy.com/' target='_blank' rel='noopener nofollow noreferer'>Mappy</a> ou " +
        "<a href='https://www.viamichelin.fr/' target='_blank' rel='noopener nofollow noreferer'>ViaMichelin</a>" +
        " une estimation du coût du voyage en carburant.",
      "Si vous étiez plusieurs à faire le trajet, une seule note de frais suffit : je renseigne simplement le nom de mes covoitureur·euses.",
    ],
    template: [
      "Date du trajet : ",
      "Lieu de départ : ",
      "Lieu d'arrivée : ",
      "Coût en carburant estimé : ",
    ],
  },
  {
    name: "Ticket de parking",
  },
  {
    name: "Ticket de péage",
  },
 */
  {
    name: "Trajet voiture de salarié·e",
    attachments: ["La carte grise du véhicule", "Une estimation du nombre de kilomètres parcourus"],
    tips: [],
    template: ["Date du trajet : ", "Lieu de départ : ", "Lieu d'arrivée : "],
    tips: [
      "Pour une estimation du nombre de kilomètres, tu peux te rendre sur <a href='https://fr.mappy.com/' target='_blank' rel='noopener nofollow noreferer'>Mappy</a> ou <a href='https://www.viamichelin.fr/' target='_blank' rel='noopener nofollow noreferer'>ViaMichelin</a>. <br>Le montant indiqué, qui suit le barème officiel, peut surprendre ; si tu souhaites demander moins mais que tu ne sais pas combien, tu peux demander par exemple la moitié (ce qui couvre déjà plus que les frais directs mais aussi une partie de l'usure du véhicule), ou bien demander seulement l'équivalent d'essence+péages.",
    ],
    additionalInputFields: [
      {label: "Nombre de kilomètres", key: "numberOfKm", inputType: "number"},
      {
        label: "Puissance administrative",
        inputType: "select",
        key: "adminPower",
        values: [
          {value: 0.529, label: "3 CV ou moins"},
          {value: 0.606, label: "4 CV"},
          {value: 0.636, label: "5 CV"},
          {value: 0.665, label: "6 CV"},
          {value: 0.697, label: "7 CV et plus"},
        ],
      },
    ],
    amountFormula: ({numberOfKm, adminPower}) => Math.round(100 * numberOfKm * adminPower) / 100,
  },
  {
    name: "Trajet voiture de bénévole",
    attachments: ["La carte grise du véhicule", "Une estimation du nombre de kilomètres parcourus"],
    tips: [],
    template: ["Date du trajet : ", "Lieu de départ : ", "Lieu d'arrivée : "],
    tips: [
      "Pour une estimation du nombre de kilomètres, tu peux te rendre sur <a href='https://fr.mappy.com/' target='_blank' rel='noopener nofollow noreferer'>Mappy</a> ou <a href='https://www.viamichelin.fr/' target='_blank' rel='noopener nofollow noreferer'>ViaMichelin</a>. <br>Ce montant est censé couvrir une partie de l'usure du véhicule, en plus des frais directs ; mais si tu souhaites demander moins, par exemple juste l'équivalent d'essence+péages, tu peux !",
    ],
    additionalInputFields: [
      {label: "Nombre de kilomètres", key: "numberOfKm", inputType: "number"},
      {
        label: "Puissance administrative",
        inputType: "select",
        key: "adminPower",
        values: [
          {value: 0.529, label: "3 CV ou moins"},
          {value: 0.606, label: "4 CV"},
          {value: 0.636, label: "5 CV"},
          {value: 0.665, label: "6 CV"},
          {value: 0.697, label: "7 CV et plus"},
        ],
      },
    ],
    amountFormula: ({numberOfKm, adminPower}) =>
      Math.round((100 * numberOfKm * adminPower) / 2) / 100,
  },
  {
    name: "Billet de train",
    attachments: ["Le justificatif ou la facture du billet acheté"],
    tips: [
      "Idéalement, j'envoie la note de frais après avoir effectué le voyage pour éviter les galères d'annulation de billet.",
    ],
    template: ["Date du trajet : ", "Lieu de départ : ", "Lieu d'arrivée : "],
    additionalInputFields: [],
    amountField: "manual",
  },
  {
    name: "Paiement covoiturage",
    template: ["Date du trajet : ", "Lieu de départ : ", "Lieu d'arrivée : "],
    additionalInputFields: [],
    amountField: "manual",
  },
  {
    name: "Achats divers",
    additionalInputFields: [],
    amountField: "manual",
  },
  // 27/11/2023 - Gauthier - Retrait du champ car le fond d'aide aux permanent⋅es est remplacé par une prime télétravail
  //  {
  //    name: "Fonds d'aide permanent·es",
  //  },
  {
    name: "Paiement de formation",
    additionalInputFields: [],
    amountField: "manual",
  },
  {
    name: "Réservation de lieu",
    additionalInputFields: [],
    amountField: "manual",
  },
];

/*
 * [ CONSEILS DE REMPLISSAGE GENERAUX ]
 * Ces conseils sont affichés à l'utilisateur·ice, mais ne sont pas imprimés sur la note de frais finale.
 */
export const refundSpecification = [
  "ℹ️ Le montant total indiqué est le maximum que tu peux demander, mais si tu souhaites demander un remboursement moindre, tu peux saisir un montant inférieur.",
];
//export const refundSpecification = [];

/*
 * [ WARNING SI LE MONTANT DEMANDE EST SUPERIEUR AUX FRAIS ]
 * Ces conseils sont affichés à l'utilisateur·ice, mais ne sont pas imprimés sur la note de frais finale.
 */
export const warningRefundTooLarge = [
  "⚠️ Tu ne peux pas demander un remboursement supérieur au montant total des frais.",
];
//export const warningRefundTooLarge = [];

/**
 * [ CONSEILS DE REMPLISSAGE GENERAUX ]
 * Ces conseils sont affichés à l'utilisateur·ice, mais ne sont pas imprimés sur la note de frais finale.
 */
export const generalTips = [
  `<strong>Une ligne, une dépense :</strong> mieux vaut faire une ligne pour chaque ticket/facture/billet,
plutôt qu'une seule pour plusieurs dépenses. C'est plus facile à re-compter pour les personnes en charge !`,
  `Lorsque c'est possible, <strong>scannez vos justificatifs en PDF</strong>. Il existe de bonnes applications libres telles que 
<a href="https://play.google.com/store/apps/details?id=com.ethereal.openscan" target="_blank" rel="noopener noreferrer nofollow">OpenScan</a> et 
<a href="https://play.google.com/store/apps/details?id=com.bretahajek.docus" target="_blank" rel="noopener noreferrer nofollow">Docus</a>. 
Si vous avez un iPhone, vous pouvez utiliser <a href="https://www.camscanner.com/user/download" target="_blank" rel="noopener noreferrer nofollow">CamScanner</a>.`,
  "Les tickets de <strong>carte bleue</strong> ne sont <strong>pas des pièces justificatives</strong>.",
];

/**
 * [ CONSEILS AFFICHÉS DANS LE MODAL ]
 * Ces conseils sont affichés dans le modal de confirmation. Ils ne sont pas imprimés sur la note de frais.
 */
export const modalTips = [
  `<strong>Un doute sur les justificatifs à fournir ?</strong> Jette un coup d'oeil au
          <a
            href="https://docs.google.com/document/d/169M9TPFNKQvg6lpUdfPoGcTuiaEaaHkSXXXYoE4LCmg/edit"
            target="_blank"
            rel="noopener noreferrer nofollow">
            <strong>document mémo</strong>
          </a>
          !`,
];

/**
 * [ INFORMATIONS DE BAS DE PAGE ]
 * Texte affiché en bas de la note de frais.
 */

/*
export const pageFooter = ["Ajouter sur l’enveloppe “à l’attention d'Émilie'”."];
*/
export const pageFooter = [""];

/**
 * [ AUTRES PETITES OPTIONS ]
 * Le nom par défaut d'une note de frais vide.
 */
export const defaultBillTitle = "Note de frais Alternatiba / ANV-COP21";

/**
 * [ INFOS COMPLÉMENTAIRES SUR L'APPLICATION EN BAS DE PAGE WEB ]
 */
export const webPageInfo = [
  `Un bug ? 🤔 Une suggestion ? 💡<br/>Envoyez un email à <a href="mailto:informatique@alternatiba.eu" class="text-white">informatique@alternatiba.eu</a> 😊`,
];

/**
 * [ MONNAIE PAR DÉFAUT ]
 *
 * Elle peut ensuite être changée au besoin en cliquant sur les euros
 */
export const defaultMoneySign = "€";
